### The Java Challenge ###

The Animals is a game with the purpose of making people go to places and take pictures of animals.
It keeps a series of logs from events occurred during the game.
To help us to learn more about your skills, we would like you to create a program in Java to process those logs.

### What is expected? ###

Given a log file, you should create:

* a ranking of animals which captured
* a ranking of animals which ran away
* a ranking of animals which were saved
* a ranking of animals that decided to follow the player

### Extras ###

The following tasks are optional, but if you decide to do them, you should create:

* a list of items gained, with its quantity, during the game play
* a list of places where an animal usually appears (consider all the events)
    * by city
    * by state
* a list of places the player prefers to play
    * by city
    * by state
* a period of time that an animal usually appears 
    * what time does it appear and what time does it disappear?
* a period of time the player prefers to play
    * what time does he start playing and what time does he stop?

### About your solution ###

* Be creative and organized
* Use Object Oriented
* You should create unit tests and try to use TDD
* You can use frameworks but we would prefer you to avoid them

### Instructions to submit  ###

* You should create a private fork in the bitbucket of your code 
* Share your fork with dev-fitec@fitec.org.br and send an e-mail to selecao@fitec.org.br along with the link
